

## Tech Challenge - Front End Developer 2022 @ CPS
Technical Challenge
Welcome to the Coding Challenge! This challenge will give us an idea about how you work. This challenge consists of creating a responsive
ReactJs Web App. Please plan around 3-4 hours for this challenge.
Prerequisites
DOs:
DO: Use Typescript or Javascript for this challenge.
DO: We will like to see how you create efficient code, follow best practices, use coding patterns and that your code is readable.
DO: Use any third-party library you want.
DONTs:
DONT: Please don’t reinvent the wheel. e.g.: you do not need to create a “networking” library from scratch, just use the library
you prefer.
DONT: Do not spend much time on the design.
DONT: Please, do not squash your commits. We want to see how your code evolves!

The problem to solve
We want an App that search movies from http://themoviedb.org and shows it in a scrollable list. You can find information about the API and how
to use it here: https://developers.themoviedb.org/3/getting-started/introduction
Must have
Create a code repo wherever you want to.
Create a README.md that explains how setup and run the App.
Create web app to be displayed on a desktop and a mobile device with a screen that:
Shows a search field to enter the search.
Shows the list of movies found. For each movie, it should show:
Title
Year
an image of the movie, if any. Otherwise, setup a placeholder.

Send us the link to download the repo once you are done.
Bonus points
Write unit tests.
Cache data for offline search.
A “load more” mechanism. It should be triggered once you are reaching the end of the current movies list.
Keep in mind
We want to see how you solve problems, structure your code, and if it is readable!
We will talk about your code in the interview:
You will be using your own computer.
We will ask you to share your screen on Teams and explain us the code
We may ask some questions about your code.
Finally, you will be required to refactor a small piece of code.

If you have any questions, please send us an eMail!

[![GitLab pipeline](https://img.shields.io/gitlab/pipeline/renatoserra/cibersecurity-movies-series)](https://gitlab.com/renatoserra/cibersecurity-movies-series/builds)

<h1 align="center">Best movies - The best movies.</h1>
 
 This project was created to implement a fundamental application using react, redux and service workers

<hr>

## Getting Started

1. Clone this repository

   ```bash
   git clone https://gitlab.com/renatoserra/movies.git
   cd movies
   ```
2. Install the npm packages

   ```bash
   npm install
   ```

3. Run the app!

   ```bash
   npm run start
   ```   
4. Run lint
   ```bash
   npm run lint
   ``` 
4. Run unit tests
   ```bash
   npm run test
   ```   
5. Run cypress tests
   ```bash
   npm run start
   npm run cypress:open
   ``` 

## Offline version
  
   this project use service workers
