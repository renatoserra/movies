## Motivation

I needed to create web application that has good redibility, scalability and performance and that can be displayed on a desktop and a mobile device.

To achieve this, a pattern was delimited.

### Library
  According to requirements I used reactjs.

## Software Architecture Pattern

This image represents the architecture that is used by the system. I have two main folders in the structure to provide a simple and robust implementation.


![API Architecture](docs/images/architecture.png)


### Cache

To optimize the speed of responses and avoid unnecessary consumption of the api and offline usability the aplication use service workers.

### Manager states

To avoid spaghettification problems very common in SPAs I am using redux library.

### Consistent 

Unit tests, cypress and lint was applied. 
