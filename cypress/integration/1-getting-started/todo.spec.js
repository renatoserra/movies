/// <reference types="cypress" />

describe('Show movies', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  });

  it('displays two todo items by default', () => {
    cy.get('.input-desktop').type('home');
    cy.get(':nth-child(1) > .card-img-top').should('be.exist');
  });
});
