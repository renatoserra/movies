/* eslint-disable no-restricted-globals */
const cacheName = 'v1';
const resourcesToPrecache = [
  '/',
  'index.html',
  'settings.css',
  'bundle.js',
  'https://api.themoviedb.org/3/search/movie?api_key=2e25eaba55b240b517a7f5c89daf7b7a&language=en-US&query=hacker&page=1&include_adult=false',
];

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(cacheName).then((cache) => {
      cache.addAll(resourcesToPrecache);
    }),
  );
});

self.addEventListener('fetch', (event) => {
  event.respondWith(
    caches.open(cacheName).then((cache) => {
      return cache.match(event.request).then((response) => {
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      });
    }),
  );
});
