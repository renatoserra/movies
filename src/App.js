import './App.css';
import Carousel from './core/components/carousel/Carousel';
import Header from './core/layout/Header';
import Footer from './core/layout/Footer';
import Media from './core/layout/Media';

function App() {
  const brand = 'Best Movies';
  const placeholder = 'What do you want to watch?';
  return (
    <div className="App">
      <Header brand={brand} placeholder={placeholder} />
      <Carousel />
      <Media />
      <Footer />
    </div>
  );
}

export default App;
