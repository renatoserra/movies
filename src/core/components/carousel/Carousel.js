import './Carousel.css';
import CarouselInner from './carousel-inner/Carousel.inner';

function Carousel() {
  return (
    <section className="section-banner">
      <div id="carousel-cyber" className="carousel slide" data-bs-ride="carousel">
        <div className="carousel-indicators">
          <button type="button" data-bs-target="#carousel-cyber" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1" />
        </div>
        <CarouselInner />
        <button className="carousel-control-prev" type="button" data-bs-target="#carousel-cyber" data-bs-slide="prev">
          <span className="carousel-control-prev-icon" aria-hidden="true" />
          <span className="visually-hidden">Previous</span>
        </button>
        <button className="carousel-control-next" type="button" data-bs-target="#carousel-cyber" data-bs-slide="next">
          <span className="carousel-control-next-icon" aria-hidden="true" />
          <span className="visually-hidden">Next</span>
        </button>
      </div>
    </section>
  );
}

export default Carousel;
