import { render } from '@testing-library/react';
import Carousel from './Carousel';

it('Should exist section-banner className', () => {
  const { container } = render(<Carousel />);
  const tag = container.querySelector('.section-banner');
  expect(tag).toBeTruthy();
});

it('Should exist carousel-cyber className', () => {
  const { container } = render(<Carousel />);
  const tag = container.querySelector('#carousel-cyber');
  expect(tag).toBeTruthy();
});

it('Should exist carousel-control-prev className', () => {
  const { container } = render(<Carousel />);
  const tag = container.querySelector('.carousel-indicators');
  expect(tag).toBeTruthy();
});

it('Should exist carousel-control-prev className', () => {
  const { container } = render(<Carousel />);
  const tag = container.querySelector('.carousel-control-prev');
  expect(tag).toBeTruthy();
});

it('Should exist carousel-control-prev-icon className', () => {
  const { container } = render(<Carousel />);
  const tag = container.querySelector('.carousel-control-prev-icon');
  expect(tag).toBeTruthy();
});

it('Should exist visually-hidden className', () => {
  const { container } = render(<Carousel />);
  const tag = container.querySelector('.visually-hidden');
  expect(tag).toBeTruthy();
});

it('Should exist carousel-control-next className', () => {
  const { container } = render(<Carousel />);
  const tag = container.querySelector('.carousel-control-next');
  expect(tag).toBeTruthy();
});

it('Should exist carousel-control-next-icon className', () => {
  const { container } = render(<Carousel />);
  const tag = container.querySelector('.carousel-control-next-icon');
  expect(tag).toBeTruthy();
});

it('Should exist visually-hidden className', () => {
  const { container } = render(<Carousel />);
  const tag = container.querySelector('.visually-hidden');
  expect(tag).toBeTruthy();
});