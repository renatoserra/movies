import './Carousel.inner.css';

function CarouselInner() {
  return (
    <div className="carousel-inner">
      <div className="carousel-item active">
        <img src="assets/img/banner.jpg" className="d-block w-100" alt="movie info" />
        <section className="info-media">
          <h1 className="title-info">Hacker (2019)</h1>
          <div className="caption-media">
            <figure className="idbm-info">
              <img src="assets/img/idbm.svg" alt="idbm" />
              <span className="idbm-cotation">86.0/100</span>
            </figure>
            <p className="description-media">
              13-year-old Benjamin discovers that his mother didn’t die in an accident as
              he was led to believe.
              The trail leads to high-ranking officials in the Danish Secret Service.
              Trust no one!, he is told.
            </p>
          </div>
        </section>
      </div>
    </div>
  );
}

export default CarouselInner;
