import { render } from '@testing-library/react';
import CarouselInner from './Carousel.inner';

it('Should exist carousel-inner className', () => {
  const { container } = render(<CarouselInner />);
  const tag = container.querySelector('.carousel-inner');
  expect(tag).toBeTruthy();
});

it('Should exist d-block className', () => {
  const { container } = render(<CarouselInner />);
  const tag = container.querySelector('.d-block');
  expect(tag).toBeTruthy();
});

it('Should exist info-media className', () => {
  const { container } = render(<CarouselInner />);
  const tag = container.querySelector('.info-media');
  expect(tag).toBeTruthy();
});

it('Should exist idbm-info className', () => {
  const { container } = render(<CarouselInner />);
  const tag = container.querySelector('.idbm-info');
  expect(tag).toBeTruthy();
});

it('Should exist idbm-info img', () => {
  const { container } = render(<CarouselInner />);
  const tag = container.querySelector('.idbm-info img');
  expect(tag).toBeTruthy();
});

it('Should exist idbm-cotation clssName', () => {
  const { container } = render(<CarouselInner />);
  const tag = container.querySelector('.idbm-cotation');
  expect(tag).toBeTruthy();
});

it('Should exist description-media className', () => {
  const { container } = render(<CarouselInner />);
  const tag = container.querySelector('.description-media');
  expect(tag).toBeTruthy();
});

