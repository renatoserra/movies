import './Galery.css';

function managerUrl(item) {
  const url = `https://image.tmdb.org/t/p/w300/${item.poster_path}`;
  const fallbackImag = 'assets/img/fallback.jpeg';
  return item.poster_path ? url : fallbackImag;
}

function Galery({ mediaResults }) {
  const media = mediaResults.map((item) => (
    Card(item)
  ));
  return ifNotFound(media);
}

function ifNotFound(media) {
  if (media.length === 0) {
    return <p className="not-found">Movie not found</p>;
  }
  return media;
}

function Card(item) {
  const fallbackImag = 'assets/img/fallback.jpeg';
  const errorLoadingImag = () => `src=${fallbackImag}`;
  return (
    <div className="card" key={item.id}>
      <img src={managerUrl(item)} onError={errorLoadingImag} className="card-img-top" alt="movies" />
      <div className="card-body">
        <span className="release">{item.release_date}</span>
        <h5 className="card-title">{item.title}</h5>
        <figure className="idbm-info">
          <img src="assets/img/idbm.svg" alt="idbm" />
          <span className="idbm-cotation">
            {60 + item.vote_average}
            / 100
          </span>
        </figure>
      </div>
    </div>
  );
}

export default Galery;
