import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import uuid from 'react-uuid';

function CarouselInner(props) {
  const { socialIcons } = props;
  return (
    <div>
      {
        socialIcons.map((icons) => (
          <button type="button" key={uuid()} className="social-icon">
            <FontAwesomeIcon icon={icons} />
          </button>
        ))
       }
    </div>
  );
}

export default CarouselInner;
