import {
  faFacebookF, faTwitter, faYoutube, faTelegram,
} from '@fortawesome/free-brands-svg-icons';

import CarouselInner from '../components/social-icons/Social.icons';
import './Footer.css';

function Footer() {
  const socialIcons = [faFacebookF, faTwitter, faTelegram, faYoutube];
  return (
    <footer className="footer">
      <div className="container-footer">
        <article className="social-group">
          <CarouselInner socialIcons={socialIcons} />
        </article>
        <article className="infocontainer">
          <span className="info">Conditions of Use</span>
          <span className="info">Privacy & Policy</span>
          <span className="info">Press Room</span>
        </article>
      </div>
    </footer>
  );
}

export default Footer;
