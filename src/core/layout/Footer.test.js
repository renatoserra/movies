import '@testing-library/jest-dom';
import { render, screen } from '../../infrastructure/test';
import Footer from './Footer';

it('Should exist text Conditions of Use', () => {
  render(<Footer />);
  const info = screen.queryByText('Conditions of Use');
  expect(info).toBeInTheDocument();
});

it('Should exist text Privacy & Policy', () => {
  render(<Footer />);
  const info = screen.queryByText('Privacy & Policy');
  expect(info).toBeInTheDocument();
});

it('Should exist text Press Room', () => {
  render(<Footer />);
  const info = screen.queryByText('Press Room');
  expect(info).toBeInTheDocument();
});
