import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import { getMedia } from '../../infrastructure/store/action/media.action';

import './Header.css';

const mapStateToProps = (props) => {
  return {
    mediaItems: props.mediaReducer,
  };
};

const mapDispatToProps = (dispatch) => {
  return {
    loadMedia: (queryParams) => dispatch(getMedia(queryParams)),
  };
};

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.getSearchTerm = this.getSearchTerm.bind(this);
  }

  getSearchTerm(eventTerm) {
    const { value } = eventTerm.target;
    if (value.length > 3 || value.length === 0) {
      const queryParams = { page: 1, query: value };
      this.props.loadMedia({ ...queryParams });
    }
  }

  render() {
    return (
      <header>
        <div className="container-logo-brand">
          <img src="assets/img/logo.svg" alt="logo" className="logo" />
          <span className="brand-title">{this.props.brand}</span>
        </div>
        <div className="header-search">
          <input className="input-desktop" type="text" onChange={this.getSearchTerm} placeholder={this.props.placeholder} name="search" />
          <input className="input-mobile" type="text" onChange={this.getSearchTerm} placeholder="Search" name="search" />
          <button type="button" className="search-btn">
            <FontAwesomeIcon icon={faSearch} />
          </button>
        </div>
      </header>
    );
  }
}

export default connect(mapStateToProps, mapDispatToProps)(Header);
