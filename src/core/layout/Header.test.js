import '@testing-library/jest-dom';
import {
  render, store, Provider, screen, fireEvent,
} from '../../infrastructure/test';
import Header from './Header';

describe('Header component', () => {
  beforeEach(() => {
    const brand = 'Mock brand';
    const placeholder = 'placeholder mock';
    render(
      <Provider store={store}>
        <Header brand={brand} placeholder={placeholder} />
      </Provider>,
    );
  });

  it('Should exist text Mock brand', () => {
    const text = screen.queryByText('Mock brand');
    expect(text).toBeInTheDocument();
  });

  it('Should exist text placeholder mock', () => {
    const text = screen.queryByPlaceholderText('placeholder mock');
    expect(text).toBeInTheDocument();
  });

  it('Should exist text placeholder mock', () => {
    const text = screen.queryByPlaceholderText('placeholder mock');
    fireEvent.change(text, { target: { value: 'movies mock' } });
  });
});
