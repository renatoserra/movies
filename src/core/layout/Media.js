import { connect } from 'react-redux';
import React from 'react';
import Galery from '../components/galery/Galery';
import { getMedia } from '../../infrastructure/store/action/media.action';
import './Media.css';

const mapStateToProps = (props) => {
  return {
    mediaItems: props.mediaReducer,
  };
};

const mapDispatToProps = (dispatch) => {
  return {
    loadMedia: (queryParams) => dispatch(getMedia(queryParams)),
  };
};

class Media extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstPage: true,
      queryParams: {
        query: null,
        page: 1,
      },
      mediaOld: [],
    };
    this.iteratePage = this.iteratePage.bind(this);
  }

  componentDidMount() {
    this.loadMedia();
    this.scrollEndEvent();
  }

  loadMedia() {
    const { queryParams } = this.state;
    this.props.loadMedia({ ...queryParams });
  }

  scrollEndEvent() {
    const intersectionObserver = new IntersectionObserver((entries) => {
      if (entries.some((entry) => entry.isIntersecting)) {
        this.iteratePage();
      }
    });
    intersectionObserver.observe(document.querySelector('.scroll-end'));
    return () => intersectionObserver.disconnect();
  }

  iteratePage() {
    if (!this.state.firstPage) {
      const actualPage = this.state.queryParams.page;
      this.setState(() => ({
        firstPage: false,
        queryParams: { page: actualPage + 1, query: 'hacker' },
      }));
      this.setState(() => ({
        mediaOld: [...this.props.mediaItems.media],
      }));
      this.loadMedia();
      this.props.mediaItems.media = this.cloneMedia(
        this.props.mediaItems.media,
        this.state.mediaOld,
      );
    }
    this.setState(() => ({
      firstPage: false,
    }));
  }

  cloneMedia(actualMedia, oldMedia) {
    if (oldMedia.length > 0) {
      return [...this.props.mediaItems.media, ...this.state.mediaOld];
    }
    return actualMedia;
  }

  render() {
    return (
      <div className="galery">
        <Galery mediaResults={this.props.mediaItems.media} />
        <hr className="scroll-end" />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatToProps)(Media);
