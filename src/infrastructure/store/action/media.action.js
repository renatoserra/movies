import * as type from '../types';

export function getMedia(params) {
  return {
    type: type.GET_MEDIA_REQUESTED,
    params,
  };
}

export function getMediaFailed() {
  return {
    type: type.GET_MEDIA_FAILED,
  };
}
