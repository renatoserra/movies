import { combineReducers } from 'redux';
import mediaReducer from './media.reducer';

const rootReducer = combineReducers({
  mediaReducer,
});

export default rootReducer;
