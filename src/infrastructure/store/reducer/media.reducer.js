import * as type from '../types';

const initialState = {
  loading: false,
  error: null,
  media: [],
};

export default function mediaReducer(state = initialState, action) {
  switch (action.type) {
    case type.GET_MEDIA_REQUESTED:
      return {
        ...state,
        loading: true,
      };
    case type.GET_MEDIA_SUCCESS:
      return {
        ...state,
        loading: false,
        media: action.media,
      };
    case type.GET_MEDIA_FAILED:
      return {
        ...state,
        loading: false,
        error: action.message,
      };
    default:
      return state;
  }
}
