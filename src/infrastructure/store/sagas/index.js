import { all } from 'redux-saga/effects';
import mediaSaga from './media.saga';

export default function* rootSaga() {
    yield all([
        mediaSaga()
    ])
} 