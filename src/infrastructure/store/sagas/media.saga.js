import { call, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import * as type from '../types';
import { urlApi, apiKey } from '../../environment';

const defaultSearchTerm = 'hacker';

function getMedia(queryParams) {
  const { query, page } = queryParams.params;
  const queryTerm = query || defaultSearchTerm;
  const params = {
    api_key: apiKey, language: 'en-US', query: queryTerm, page, include_adult: false,
  };
  return axios.get(urlApi, { params });
}

function* fetchMedia(params) {
  try {
    const media = yield call(getMedia, params);
    yield put({ type: type.GET_MEDIA_SUCCESS, media: media.data.results });
  } catch (error) {
    yield put({ type: type.GET_MEDIA_FAILED, message: error.message });
  }
}

function* mediaSaga() {
  yield takeLatest(type.GET_MEDIA_REQUESTED, fetchMedia);
}

export default mediaSaga;
