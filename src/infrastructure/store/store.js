/* eslint-disable no-underscore-dangle */
/* eslint-disable no-undef */
import { compose, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducer';
import rootSaga from './sagas';

const sagaMidleware = createSagaMiddleware();

const store = compose(
  applyMiddleware(sagaMidleware),
)(createStore)(rootReducer);

sagaMidleware.run(rootSaga);
export default store;
